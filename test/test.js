var algorithm = require("../app/algorithm");

describe("Test 1", () => {
   
    it("test - 01", () => {
        const output = algorithm.getResult([0,1,1,1,3,0]);
        console.log(output);
    });

    it("test - 02", () => {
        const output = algorithm.getResult([0,9]);
        console.log(output);
    });
});