exports.getResult = (array) => {
    let result = [];

    for(let i = 0; i < array.length; i++){
        let count = 0;
        for(let j = 0; j < array.length; j++){
            if(array[j] == i){
                count++;
            }
        }
        result[i] = count;
    }

    return result;
}