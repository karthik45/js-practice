# Singapore Assignment

The tech stack used for this project are:

    1. JAVASCRIPT as the programming language
    2. MOCHA as test framework
    3. CHAI as assertion library

## Getting Started

Running Tests

    1. Clone the project and build it using *npm i*
    2. Run the tests using *npm run test*
    3. Check the results in console.